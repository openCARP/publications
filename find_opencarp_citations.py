#!/usr/bin/env python
import os
import sys
import time
import smtplib
import argparse
import fileinput
from datetime import datetime

from publication_citations.scholarpublication import ScholarPublication
from publication_citations.bib_csv import bibstring_to_csv
from publication_citations.finddois import add_dois

import pubmed_automatic_carpentry


SCRIPT_DIR = os.path.realpath(os.path.dirname(__file__))
OUTPUT_NAME = os.path.join(SCRIPT_DIR, 'openCARP')
OUTPUT_DOIS_FILENAME = os.path.join(SCRIPT_DIR, 'opencarp_dois.txt')
MANUAL_BIBLIO_FILENAME = os.path.join(SCRIPT_DIR, 'manual_biblio.bib')
BLACKLIST_DOI_FILENAME = os.path.join(SCRIPT_DIR, 'blacklist_dois.txt')
BLACKLIST_TITLE_FILENAME = os.path.join(SCRIPT_DIR, 'blacklist_titles.txt')
PUBLICATION_TITLES = ["openCARP: An open sustainable framework for in-silico cardiac electrophysiology research", "The openCARP simulation environment for cardiac electrophysiology"]


def main(): 
    """
    This function creates the Bibtex file containing the publications citing the openCARP publication
    and pushes it to the git repository.
    If the update fails, a notification email is sent.
    """
    parser = argparse.ArgumentParser(description='Automatically update bibtex files with openCARP citations.')
    parser.add_argument('--push', action='store_true', help='Push changes to remote repository')
    parser.add_argument('--skip-carpentry', action='store_true', help='Skip updating carpentry references')
    args = parser.parse_args()

    print(datetime.now().strftime("%d/%m/%Y %H:%M:%S"), ": starting to fetch publications...")
    try:
        update_citations_list(PUBLICATION_TITLES,
                              out=OUTPUT_NAME,
                              out_dois=OUTPUT_DOIS_FILENAME,
                              manual_biblio=MANUAL_BIBLIO_FILENAME,
                              blacklist_dois_file=BLACKLIST_DOI_FILENAME,
                              blacklist_titles_file=BLACKLIST_TITLE_FILENAME)
    except Exception as exc:
        send_warning_email()
        raise exc

    if args.push:
        print("Pushing openCARP changes to remote repository...")
        push_bibtex_file(OUTPUT_NAME + '.bib')

    if not args.skip_carpentry:
        print("Updating CARP/carpentry citations...")
        pubmed_automatic_carpentry.main()
        if args.push:
            print("Pushing CARP changes to remote repository...")
            push_bibtex_file('CARP.bib')


def update_citations_list(pub_titles, out='citing_biblio', out_dois='dois.txt', manual_biblio=None, blacklist_dois_file='blacklist_dois.txt', blacklist_titles_file='blacklist_titles.txt'):
    """
    This function creates or updates the BibTex file containing all publications which cite the publications whose titles
    are given as a list as parameter.
    It also looks for the DOIs of the publications. The DOIs are added to the output BibTex file and are also listed in
    the `out_dois` file.
    A manual list of publications can be provided and will be merged to the found citing publications.
    :param pub_title: list of publications titles
    :param out: Basename for the output files
    :param out_dois: Name of the DOIs output file
    :param manual_biblio: Name of the BibTex file containing additional bibliography
    """
    # Define the name of the produced csv file
    csvfile = out + '.csv'
    doi_list = []
    is_first_publication = True
    with open(blacklist_dois_file) as file:
        blacklist_dois = [line.strip() for line in file]
    with open(blacklist_titles_file) as file:
        blacklist_titles = [line.strip() for line in file]
    for pub_title in pub_titles:
        # Find the publication on Google Scholar
        print("Checking for citations of \"%s\"..."%pub_title)
        pub = ScholarPublication(pub_title, blacklist_titles=blacklist_titles, blacklist_dois=blacklist_dois)
        # Convert the list of the citing papers to csv
        if pub.citations:
            bibstring_to_csv(pub.citations_bibstring, out=csvfile)
            # Add the DOIs to the csv file
            doi_list += add_dois(csvfile)
            # Add the DOIs to the bibtex string
            pub.add_citations_doi_from_csv(csvfile)
        if is_first_publication:
            first_pub = pub
            is_first_publication = False
        else:
            first_pub.merge_bibstring_with_citations(pub.citations_bibstring)
    # Merge manually provided bib file
    first_pub.merge_bibfile_with_citations(manual_biblio)
    # Write bibtex file of citing papers
    first_pub.write_citations_bibfile(out + '.bib')

    # turn scholarly's pub_year fields into year
    with fileinput.FileInput(out + '.bib', inplace=True) as file:
        for line in file:
            print(line.replace('pub_year = ', 'year = '), end='')

    # Write list of found DOIs in a file (used by the CARPentry script)
    write_list_in_file(doi_list, out=out_dois)


def write_list_in_file(l, out='out_list.txt'):
    """
    Write the element of a list in a file, one element per line.
    :param l: list to write
    :param out: output file name
    """
    with open(out, 'w') as f:
        for elem in l:
            f.write(elem + '\n')


def send_warning_email():
    """
    Sends a warning e-mail to Marie (please modify it if you use this function).
    """

    sender_email = "pubalert@ibt.kit.edu"
    receiver_email = "marie.houillon@kit.edu"

    message = """\
    Subject: [openCARP citations] Warning ! Retrieving openCARP citations failed.

    The search of openCARP citations launched on """
    message += datetime.now().strftime("%d/%m/%Y %H:%M:%S") + " failed."

    try:
        smtpObj = smtplib.SMTP('smtp.kit.edu')
        smtpObj.sendmail(sender_email, receiver_email, message)
        print("Successfully sent warning email")
    except smtplib.SMTPException:
        print("Error: unable to send warning email")

def push_bibtex_file(bibfile):
    """
    Commits and pushes bibfile to the git repository.
    :param bibfile: name of the BibTex file
    """
    os.system("git pull origin HEAD:master")
    os.system("git commit -m 'Automatically updated citations BibTex file' '{}'".format(bibfile))
    os.system("git push origin HEAD:master")


if __name__ == '__main__':
    main()
