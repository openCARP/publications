WARNING: this repository is no longer in use. See https://git.opencarp.org/openCARP/opencarp-citations instead.

# Collection of publications using openCARP (and its predecessor)
* openCARP.bib
* CARP.bib

Collection using a semi-automated way (see organizational#51).

## Collecting openCARP citations
By running `find_opencarp_citations.py`, the publications citing the openCARP papers are collected and the `openCARP.bib` file is updated, committed and pushed to the repository.
As it is currently run automatically, an e-mail is sent if the operation fails.

Warning: the e-mail addresses in this script have to be modified before it can be used.
No email is trying to be sent if the `.pswd` file with the account credentials is not present.

The list of the publications that should be excluded is given in `citing_blacklist.csv`.
Please note that publications can't be excluded using their DOIs for now!

The manually-provided list of publications using OpenCARP is in `manual_biblio.bib`

The script also produces an output file named `opencarp_dois.txt` which gathers the DOIs of the publications citing openCARP which were found.
This file is used as a blacklist in the script collecting the publications using CARP/carpentry (see below).

### Installation
This script depends on [scholarly](https://pypi.org/project/scholarly/), [pybtex](https://pypi.org/project/pybtex/) and [levenshtein](https://pypi.org/project/levenshtein/).
You can install them by running `pip install -r requirements.txt`.

### Technical background
We use Google Scholar to collect citing papers. Google scholar does not return DOIs unfortunately, therefore we have to get them from Crossref by matching on the title. The python module `scholarly` does that job and also takes care of putting in sleeps between the calls to Google Scholar to try to avoid running into the limit for number of requests.
* The Crossref API cannot be used in the first place unfortunately because the `cited_by` key can only be queried by crossref members (which one can only become if one contributes data to Crossref as a publisher).
* We do not want to rely on Pubmed as to not exclude studies that are not listed there (e.g. several conference proceedings).

## Collecting CARP/carpentry citations
In order to collect CARP/carpentry citations, the `pubmed_automatic_carpentry.py` script has to be run.

In particular, every DOI included in the `opencarp_dois.txt` file is excluded from this collection.

## Transferring bibtex to the webpage
Automatically done in a CI job based on this pipeline: https://git.opencarp.org/openCARP/openCARP-CI/-/blob/master/opencarp_ci/run_bibtex_pipeline.py
