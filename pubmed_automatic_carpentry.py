#!/usr/bin/env python
"""
Automatically generates html, json or bibtex file with publications for carpentry (openCARP.org)
"""
try:
    from urllib.request import urlopen
    from urllib.error import HTTPError
    import urllib.request
except ImportError:
    from urllib2 import urlopen

import copy
import json
import os

class PubMedAccess(object):
    def __init__(self, search_api = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?",
                 summary_api = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?",
                 database = 'pubmed',       # to narrow the search down to the pubmed DB only
                 returnmode = 'json',       # to have a JSON string in response and not an XML
                 returntype = 'abstract',   #
                 returnmax = 5000,          # to obtain a maximum of 1k results
                 sort = 'pub+date',         # otherwise 'relevance'
                 authors = []):             # ['Plank+Gernot', 'Augustin+Christoph', 'Vigmond+Edward']):

        self._search_api = search_api
        self._summary_api = summary_api
        self._database = database
        self._returnmode = returnmode
        self._returntype = returntype
        self._returnmax = returnmax
        self._sort = sort
        self._search_term = None            # full search term
        self._authors = authors
        self._publications = []


    def query_db(self, addauthors = None):
        """
        Query PubMed database for articles based on given default authors
        """
        search_term = '{}db={}&retmode={}&retmax={}&sort={}'.format(self._search_api, self._database,
                                                                    self._returnmode, self._returnmax, self._sort)
        if addauthors is not None:
            self._authors.extend(addauthors)

        if self._authors is not None:
            search_term = search_term + '&term='
            for index, author in enumerate(self._authors):
                if index:
                    search_term = search_term + '%20OR%20'
                search_term = search_term + '{}[Author]'.format(author)
        self._search_term = search_term

        response = urlopen(search_term)
        pmids_str = response.read().decode('utf-8')
        pmids_json = json.loads(pmids_str)
        pmids = pmids_json['esearchresult']['idlist']

        self.__retrieve_summary(pmids) # collect entire citations based on pmids


    def __retrieve_summary(self, pmids):
        """
        Get publications based on PMIDs
        """
        if not pmids:
            print('Could not find anything with your search parameters!')
            return

        summary_term = '{}db={}&retmode={}&rettype={}'.format(self._summary_api, self._database,
                                                             self._returnmode, self._returntype)

        # retrieve summary in chunks otherwise HTTP request fails
        _pmids = pmids.copy()
        summary_dict = {}

        while len(_pmids):
            # convert list of UIDs to string
            _summary_term = summary_term + '&id=' + ','.join(_pmids[:250])
            del _pmids[:250] # remove processed elements from next iteration

            response = urlopen(_summary_term)
            _summary_str = response.read().decode("utf-8")
            _summary_json = json.loads(_summary_str)
            _summary_dict = _summary_json['result']

            # append to global summary
            if not summary_dict:
                summary_dict = _summary_dict.copy()
            else:
                # append to previous result
                summary_dict['uids'].extend(_summary_dict['uids'])
                for key, value in _summary_dict.items():
                    if 'uids' in key:
                        continue
                    summary_dict[key] = value

        # reduce this summary to the essentials
        self._publications = self.__reformat_summary(summary_dict)


    def __reformat_summary(self, summary):
        """
        list of publication records
        :param summary:
        :return:
        """
        publications = []

        for uid in summary['uids']:
            record = summary[uid]

            publication = {}
            # get year of publication
            publication['year']    = record['pubdate'][0:4]
            # get authors
            publication['authors'] = []
            for author in record['authors']:
                publication['authors'].append(author['name'])
            # get paper title
            publication['title'] = record['title']
            # get journal
            publication['journal'] = record['source']
            # get PubMed ID
            publication['PMID'] = record['uid']
            # get DOI
            publication['DOI'] = ""
            for articleid in record['articleids']:
                if articleid['idtype'] == 'doi':
                    publication['DOI'] = articleid['value']
            # get issue
            publication['issue'] = record['issue']
            # get volume
            publication['volume'] = record['volume']
            # get pages
            publication['pages'] = record['pages']
            #

            # append publication to list
            publications.append(publication)

        # return all results
        return publications


    def revise_citations(self, blacklist = None, authorpools = None):
        """
        Make sure we have found carpentry papers of the CCL group and no others
        """
        rejections_lst = [] # list of records to be rejected
        for index, publication in enumerate(self._publications):
            rejectFLAG = False

            if "Prassl AJ" in publication['authors']:
                pause = 1

            # --- QUERY AUTHORPOOLS -------------------------------------------
            if authorpools is not None:
                rejectFLAG = True

                # is one of the current authors part of the user defined author pool
                for author_p in authorpools:
                    authors   = set(publication['authors'])
                    authors_p = set(author_p)

                    matches = authors.intersection(authors_p)

                    if len(matches) > 1:
                        # at least two people of one pool published together some work
                        rejectFLAG = False
                        break

                if rejectFLAG:
                    rejections_lst.append(index)
                    send_cmdl_note(publication, 'Removed record due to inconsistent authorpool.')
                    continue


            # --- QUERY BLACKLIST ---------------------------------------------
            if blacklist is not None:
                # filter outdated records
                if isinstance(publication['year'], int) and int(publication['year']) < blacklist['year']:
                    #send_cmdl_note(publication, 'Removed record due to publication year: {}.'.format(publication['year']))
                    rejections_lst.append(index)
                    continue

                # filter wrong authors
                for author in publication['authors']:
                    if author in blacklist['authors']:
                        send_cmdl_note(publication, 'Removed record due to unknown author.')
                        rejections_lst.append(index)
                        rejectFLAG = True
                        break
                if rejectFLAG:
                    continue

                # filter by doi
                for doi in blacklist['dois']:
                    if doi in publication['DOI']:
                        send_cmdl_note(publication, 'Removed record due to blacklisting of doi.')
                        rejections_lst.append(index)
                        rejectFLAG = True
                        continue
                if rejectFLAG:
                    continue

        # finally delete suspicious data sets
        for index in reversed(rejections_lst):
            self._publications.pop(index)
        return


    def export_html(self, html_file, w_year=True):
        """
        print publications in html_format
        """
        if not self._publications:
            print('Did not find any publications with your search patterns!')
            exit(0)

        html_f = open(html_file, "w")
        html_f.write(html_header())
        old_year = self._publications[0]['year']
        if w_year:
            html_f.write('\n\n<h2 id="' + str(old_year) + '">' + str(old_year) +'</h2>\n')

        for publication in self._publications:
            year = publication['year']
            if year != old_year and w_year:
                html_f.write('\n\n<!-- ' + str(year) + ' ##################### -->\n')
                html_f.write('<h2 id="' + str(year) + '">' + str(year) +'</h2>\n')
            old_year = year

            html_f.write('<strong>' + publication['title'] + '</strong><br>\n')
            html_f.write(', '.join(publication['authors']) + " ")
            html_f.write("(" + str(publication['year']) + ")\n")
            html_f.write("<i>" + publication['journal'] + "</i>, ")
            if publication['volume']:
                html_f.write('<strong>' + publication['volume'] + '</strong>')
                if publication['issue']:
                    html_f.write("(" + publication['issue'] + ")")
                if publication['pages']:
                    html_f.write(": " + publication['pages'] + ". <br>\n")
            else:
                html_f.write('<strong>In Press.</strong><br>\n')

            pmid_url = "http://www.ncbi.nlm.nih.gov/pubmed/" + publication['PMID']
            html_f.write('PMID: <a target="_blank" rel="noopener noreferrer" href="'
                         + pmid_url + '">' + publication['PMID'] + '</a>')
            if publication['DOI']:
                doi_url = "https://www.doi.org/" + publication['DOI']
                html_f.write(',\nDOI: <a target="_blank" rel="noopener noreferrer" '
                             + 'href="' + doi_url + '">' +
                             publication['DOI'] + '</a>\n')
            html_f.write("<br><br>\n\n")
        html_f.write(html_footer())
        return


def send_cmdl_note(publication, msg):
    """
    Send a message to the commandline notifying the user that something was wrong with a processed publication record
    :param publication:
    :param msg:
    :return: None
    """
    print('"{}"'.format(publication['title']))
    print('{}'.format(','.join(publication['authors'])))
    print('{}\n'.format(msg))


def export_publications(_publications, exp_type='json'):
    """ Export Publications"""
    publications = copy.deepcopy(_publications)
    json_list = []
    blacklist = ['include']

    filename = 'publications.' + exp_type
    try:
        os.remove(filename)
    except OSError:
        pass

    # ---JSON EXPORT-----------------------------------------------------------
    if exp_type == 'json':
        for pub in publications:
            if not pub['include']:
                continue
            for item in blacklist:
                if item in pub:
                    del pub[item]
            json_list.append(pub)

        with open("publications.json", 'a+') as json_file:
            json.dump(json_list, json_file, sort_keys=False, indent=2,
                      ensure_ascii=False)

    # ---BIBTEX EXPORT---------------------------------------------------------
    elif exp_type == 'bib':
        base_url = 'http://dx.doi.org/'
        for pub in publications:

            if not pub['DOI']:
                print('Found empty DOI in \n{}'.format(pub))
                continue

            url = os.path.join(base_url, pub['DOI'])
            req = urllib.request.Request(url)
            req.add_header('Accept', 'application/x-bibtex')
            try:
                with urllib.request.urlopen(req) as u_file:
                    bibtex = u_file.read().decode()

                if not bibtex.startswith('@'):
                    print('Found invalid bibtex entry: {}'.format(pub['DOI']))
                    print(bibtex)
                    continue

                with open("publications.bib", 'a+') as bib_file:
                    bib_file.write(bibtex)
                    bib_file.write('\n\n')
            except HTTPError as err:
                if err.code == 404:
                    print('DOI not found.')
                else:
                    print('Service unavailable.')

def html_header():
    """ get html header for CCL Page format"""
    header = '''<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/site.webmanifest">
    <link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Publications@CCL</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <script src="js/jquery.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/dropdown.js"></script>
    <noscript>
      <link rel="stylesheet" href="css/skel.css" />
      <link rel="stylesheet" href="css/style.css" />
      <link rel="stylesheet" href="css/style-xlarge.css" />
    </noscript>
  </head>
  <body id="top">

    <!-- Header -->
    <header id="header" class="skel-layers-fixed">
      <h1>
      <a href="#" class="image header"><img src="images/carp_symbol.png" alt="" /></a>
      <a href="#">CCL</a></h1>
      <nav id="nav">
        <ul>
          <li><a href="index.html" class="button">Home</a></li>
          <li><a href="research.html" class="button">Research</a></li>
          <li><a href="projects.html" class="button">Projects</a></li>
          <li><a href="publications.html" class="button active">Publications</a></li>
          <li><a href="people.html" class="button">People</a></li>
          <li><a href="software.html" class="button">Software</a></li>
        </ul>
      </nav>
    </header>

    <!-- ################################################################## -->
    <!-- Main -->
    <div id="main" class="wrapper style1">
      <div class="container">
        <div class="row">
          <div class="3u">
            <section class="10u">
              <h2>Publications</h2>
              <div class="dropdown">
                <button onclick="dropdownFunction()" class="button alt">Select Year</button>
                <div id="dropdown" class="dropdown-content">
                  <input type="text" placeholder="Search.." class="dropdown-search"
                         id="search" onkeyup="filterFunction()">
                  <a href="#2020">2020</a>
                  <a href="#2019">2019</a>
                  <a href="#2018">2018</a>
                  <a href="#2017">2017</a>
                  <a href="#2016">2016</a>
                  <a href="#2015">2015</a>
                  <a href="#2014">2014</a>
                  <a href="#2013">2013</a>
                  <a href="#2012">2012</a>
                  <a href="#2011">2011</a>
                  <a href="#2010">2010</a>
                  <a href="#2009">2009</a>
                  <a href="#2008">2008</a>
                  <a href="#2007">2007</a>
                  <a href="#2006">2006</a>
                  <a href="#2005">2005</a>
                  <a href="#2004">previous</a>
                </div>
              </div>
            </section>
          </div>
          <div class="9u">
            <div class="row">
              <section id="publications">
<!-- LIST OF PUBLICATIONS -->'''

    return header

def html_footer():
    """ get html footer for CCL Page format"""
    footer = '''
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ################################################################## -->
    <!-- Footer -->
    <footer id="footer">
      <div class="container">
        <div class="row double">
          <div class="8u"><div class="8u">
            <h3>Computational Cardiology Lab</h3>
            <ul class="alt">
              <li><a href="https://biophysik.medunigraz.at"
                     target="_blank" rel="noopener noreferrer">
                  Gottfried Schatz Research Center: Biophysics</a></li>
              <li><a href="https://www.medunigraz.at"
                     target="_blank" rel="noopener noreferrer">
                  Medical University of Graz</a></li>
            </ul>
          </div></div>
          <div class="4u">
            <h3>Contact</h3>
            <p>Dr. Gernot Plank<br>
              Gottfried Schatz Research Center<br>
              Division of Biophysics<br>
              Neue Stiftingtalstraße 6(MC1.D.)/IV<br>
              8010 Graz, Austria</p>
            <ul class="icons">
              <li><a href="index.html" class="icon icon-home"
                     aria-label="Home Icon">
                      <span class="label">Homepage</span></a>
              </li>
              <li><a href="javascript:window.location.href=atob(\'bWFpbHRvOmdlcm5vdC5wbGFua0BtZWR1bmlncmF6LmF0\')"
                     class="icon icon-mail-alt" aria-label="Email">
                      <span class="label">Mail</span></a>
              </li>
              <li><a href="https://goo.gl/maps/sT5sL7kYmXrf57699"
                     class="icon icon-location" aria-label="Location"
                     target="_blank" rel="noopener noreferrer">
                     <span class="label">Location</span></a>
              </li>
            </ul>
          </div>
        </div>
        <ul class="copyright">
          <li>&copy; Computational Cardiology Lab. All rights reserved.</li>
        </ul>
      </div>
    </footer>
  </body>
</html>'''
    return footer


def add_dois(blacklist, filename='opencarp_dois.txt'):
    """
    Add DOIs provided in a file (one DOI per line) to a blacklist.
    :param blacklist: the blacklist (Dict containing 'dois' as key)
    :param filename: file containing the DOIs to exclude
    :return: the updated blacklist
    """
    try:
        with open(filename, 'r') as f:
            for line in f.readlines():
                if line != '':
                    blacklist['dois'].append(line.rstrip('\n'))
    except FileNotFoundError:
        print("Warning: file of blacklisted DOIs not found.")
    return blacklist

def main():
    blacklist = {'authors': ["Plank GS", "Plank GA", "Plank GR", "Plank GL", "Plank GD", "Conkright L", "Heinritzi K",
                             "Okoye MI", "Stern DB", "Stern DH", "Stern DI", "Stern DG", "Stern DJ", "Stern DA",
                             "Stern DF", "Stem DF", "Stern DL", "Stern DM", "Stern DN", "Stern DT", "Stern DS",
                             "Stern DWF", "Stern DW", "Stern DK", "Stern DR", "de Campos FPF", "Campos FJ"],
                 'dois': ["10.1109/EMBC.2013.6609805",  # withdrawn paper
                          "10.3389/fphys.2021.646023"],  # openEP
                 'year': 2000}
    blacklist = add_dois(blacklist)
    authorpools = [['Bayer J', 'Bayer JD', 'Trayanova N', 'Trayanova NA', 'Cluitmans M', 'Roney CH'],
                   ['Plank G', 'Augustin C', 'Augustin CM', 'Gsell M', 'Gsell MAF', 'Gillette K', 'Gillette KK',
                    'Neic A', 'Prassl AJ', 'Zhou L', 'Binder JS', 'Costa CM'],
                   ['Plank G', 'Kunisch K', 'Blauer JJ', 'Swenson D', 'Trayanova N', 'Trayanova NA', 'Morgan SW'],
                   ['Vigmond E', 'Vigmond EJ', 'Bouyssier J', 'Bayer J', 'Gaur N', 'Roney C', 'Roney CH',
                    'Bayer JD', 'Beheshti M', 'Di Martino ES', 'Boukens BJ', 'Bishop MJ', 'Bishop M',
                    'Stuyvers BD', 'Bernus O', 'Kaur J', 'Kaur K', 'Behradfar E', 'Coudiere Y', 'Peskin C',
                    'Plank G', 'Clements C', 'Tsoi V'],
                   ['Plank G', 'Vigmond E', 'Vigmond EJ', 'Trayanova N', 'Trayanova NA'],
                   ['Deo M', 'Ringenberg J'],
                   ['Niederer S', 'Niederer SA', 'Plank G', 'Corrado C'],
                   ['Campos FO', 'Bishop MJ', 'Plank G'],
                   ['Vigmond E', 'Vigmond EJ', 'Plank G', 'Strocchi M', 'Deo M', 'Boyle PM', 'Clements C',
                    'Joshua Leon L', 'Leon LJ', 'Pashaei A', 'Potse M', 'Peskin CS'],
                   ['Boyle PM', 'MacLeod R', 'MacLeod RS', 'Vigmond EJ', 'Trayanova N', 'Trayanova NA', 'Roney CH'],
                   ['Deo M', 'Trayanova N', 'Trayanova NA', 'Bayer J', 'Bayer JD', 'Vigmond E'],
                   ['Loewe A', 'Wiedmann F']
                   ]

    print(blacklist)
    # Plank, Augustin
    # Vigmond, Bayer, Deo,
    # Niederer, Roney, Strocchi, Costa, ...
    # Bishop, Campos, Monaci, Gemmell, ...
    # Trayanova, Boyle, Arevalo, (Maleckar eher selten)
    # Macleod, Good, Rupp, … (einige ausgewählte rezente Arbeiten)

    pma = PubMedAccess()
    # query pubmed database für PMIDs based on default authors
    # you may add additional ones in a list: e.g. addauthor = ['Caforio+F', 'Stern+D']
    # pma.query_db()
    pma.query_db(addauthors=['Plank+Gernot', 'Vigmond+Edward'])
    pma.query_db(addauthors=['Gsell+Matthias', 'Bayer+Jason', 'Deo+Makarand'])
    pma.query_db(addauthors=['Augustin+Christoph'])
    pma.query_db(addauthors=['Neic+Aurel'])
    pma.query_db(addauthors=['Niederer+Steven'])
    pma.query_db(addauthors=['Roney+Caroline'])
    pma.query_db(addauthors=['Strocchi+Marina'])
    pma.query_db(addauthors=['Costa+Caroline+Mendonca'])
    pma.query_db(addauthors=['Bishop+Martin'])
    # pma.query_db(addauthors=['Campos+Fernando'])  # the name is too common :-(
    pma.query_db(addauthors=['Monaci+Sofia'])
    pma.query_db(addauthors=['Gemmell+Philip'])
    pma.query_db(addauthors=['Trayanova+Natalia'])
    pma.query_db(addauthors=['Boyle+Patrick'])
    pma.query_db(addauthors=['Hermenegild+Arevalo'])
    pma.query_db(addauthors=['Maleckar+Molly'])
    pma.query_db(addauthors=['MacLeod+Rob'])
    pma.query_db(addauthors=['Good+Wilson'])
    pma.query_db(addauthors=['Rupp+Lindsay'])
    pma.query_db(addauthors=['Loewe+Axel'])

    pma.revise_citations(blacklist=blacklist, authorpools=authorpools)  # wipe out suspicious records
    pma.export_html('publications.html')

    # export in bibtex format
    export_publications(pma._publications, 'bib')

    print('Done.')


# =============================================================================
if __name__ == '__main__':
    main()

