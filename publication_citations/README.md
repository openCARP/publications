# Publications Citations

This code aims at finding all publications in which a publication was cited and to
return the corresponding bibliographic file in BibTex format.
The list of the citing papers is found by querying Google Scholar thanks to the `scholarly` package.
In addition, the DOI of every paper is searched using the CrossRef API.


# Using the code

## Prerequisites
In order to use the code, please install the requirements, for example using pip:
```
pip install -r publication_citations/requirements.txt
```

## Simple use
Run the `main_example.py` script, giving the title of the publication as an argument:

```
python main.py "Left atrial hypertrophy increases P-wave terminal force through amplitude but not duration"
```

Optionally, the name of the output files can also be provided.

```
python main.py "Left atrial hypertrophy increases P-wave terminal force through amplitude but not duration" -out "citing_papers"
```

A CSV and a BibTex file are then created, containing information about the papers citing the provided publication.
In particular, the publications are searched on Google Scholar, and the DOIs of these publications are searched using the
CrossRef API. The list of found DOIs is output in a file called `dois.txt`.

Note that it is possible to provide a manually maintained BibTex file which will be merged with the BibTex file which is
automatically produced by the script, using the `-manual_biblio` option. 
In case of titles duplicates, the entry in the generated BibTex file will be erased by the one in the user-provided
BibTex file.


## Additional features
### Publications blacklist
You can add a blacklist of publications that shouldn't appear in the list of citing papers.
To do so, create a csv file named `citing_blacklist.csv` in the working directory, 
containing the titles of the papers to exclude, based on the following model:

```
title,doi
"example title",10.0.0
```

Warning 1: for now, the title has to be the exact same as the one provided by Google Scholar.

Warning 2: for now, publications can't be blacklisted using their DOIs.


# Structure of the code

## Files
- `main_example.py` is the main script which can be run to simply use this code.
- `scholarpublication.py` contains the class describing a publication and the papers which cite this publication.
  It contains the methods used to manipulate a publication fetched from Google Scholar and its citations.
- `bib_csv.py` contains the function allowing converting BibTex files to CSV files.
- `finddois.py` contains the functions allowing to retrieve the DOIs of some publications listed in a CSV file and to 
  update this file by adding the DOIs to the file.
- `pubmed_automatic_carpentry.py` searches the list of publications using CARPentry. It is independent of the rest of the files,
but uses the generated `opencarp_dois.txt` file if it exists in order to add publications to its blacklist.

## Process
When `main_example.py` is run:
- Publication information is got from Google Scholar using the `scholarly` module.
- Citing publications are got from Google Scholar and their data is stored in BibTex format.
- The BibTex file is converted to a CSV file, because the function looking for DOIs in `finddois.py` takes a CSV file as an input.
- DOIs of the publications are searched using the CrossRef API and added to the CSV file. The list of the DOIs is output in `dois.txt`.
- The CSV file is used to update the BibTex file by adding the found DOIs.
- The BibTex file is written on the disk.

Note that this particular structure is used because it is not easy to get publications DOIs from Google Scholar.
Third parties must then be used to get them (via CrossRef API), and require the use of the CSV format.

## TODOs
- Remove citing papers using DOIs provided in the blacklist
- Use the Levenshtein distance to exclude publications from the blacklist