"""
Part of this code is inspired from BibTex-to-CSV, licensed as follows:
MIT License

Copyright (c) 2019 oezguensi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import logging
import sys
import csv
import os
from pybtex.database.input import bibtex


def bibstring_to_csv(bibstring, out='biblio.csv', delimiter=',', rm_fields=None):
    """
    Reads a string in BibTex format and creates a CSV file from it.

    :param bibstring: BibTex string
    :param out: path to the output file
    :param delimiter: delimiter used in CSV file
    :param rm_fields: fields to remove from the output file
    """

    parser = bibtex.Parser()

    bib_data = parser.parse_string(bibstring)

    fields_to_remove = rm_fields if rm_fields else []

    output_fn = out + ('' if out.endswith('.csv') else '.csv')

    csv_fields = []
    csv_data = []
    # if os.path.exists(output_fn):
    #     with open(output_fn) as csvfile:
    #         csv_data.extend(list(csv.DictReader(csvfile, delimiter=delimiter)))
    #         csv_fields.extend([item for sublist in [row.keys() for row in csv_data] for item in sublist])

    try:
        csv_ids = [value['id'] for value in csv_data]
    except KeyError:
        logging.error(
            'You may have specified the wrong delimiter ("{}") to import the CSV file.'.format(delimiter))
        sys.exit(1)

    fields = list(filter(lambda x: x not in fields_to_remove,
                         set([item for sublist in
                              [value.fields.keys() for value in bib_data.entries.values()]
                              for item in sublist] + csv_fields + ['id', 'type', 'authors'])))

    rows = []
    for key, value in bib_data.entries.items():
        if value.key not in csv_ids:
            row = {}
            for field in fields:
                if field == 'id':
                    row[field] = value.key
                elif field == 'type':
                    row[field] = value.type
                elif field == 'authors':
                    row[field] = ', '.join([' '.join(
                        list(filter(None, [' '.join(person.get_part(part)) for part in ['first', 'middle', 'last']])))
                        for person in value.persons['author']])
                else:
                    row[field] = value.fields[field] if field in value.fields.keys() else None

            rows.append(row)

    rows.extend([{field: value[field] if field in value.keys() else None for field in fields} for value in csv_data])

    with open(output_fn, 'w') as output_file:
        dict_writer = csv.DictWriter(output_file, rows[0].keys())
        dict_writer.writeheader()
        dict_writer.writerows(rows)