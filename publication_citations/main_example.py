import argparse

from scholarpublication import ScholarPublication
from bib_csv import bibstring_to_csv
from finddois import add_dois

"""
This script aims at finding all the papers citing a given publication,
find their DOIs and finally write information about these papers in a
bibtex file.
"""


def parser():
    prs = argparse.ArgumentParser()
    prs.add_argument('title',
                     help='Title of the publication. '
                          'You can try with: '
                          '\"Left atrial hypertrophy increases P-wave terminal force through amplitude but not duration\"')
    prs.add_argument('-out',
                     help='Name of the output files (without extension)',
                     default='citing_biblio')
    prs.add_argument('-manual_biblio',
                     help='BibTex file providing an additional list of citing publications')
    return prs


def main():
    prs = parser()
    args = prs.parse_args()

    update_citations_list(args.title, out=args.out, manual_biblio=args.manual_biblio)


def update_citations_list(pub_title, out='citing_biblio', out_dois='dois.txt', manual_biblio=None):
    """
    This function creates or update the BibTex file containing all publications which cites the publication whose title
    is given as a parameter.
    It also looks for the DOIs of the publications. The DOIs are added to the output BibTex file and are also listed in
    the `out_dois` file.
    A manual list of publications can be provided and will be merged to the found citing publications.
    :param pub_title: Publication title
    :param out: Basename for the output files
    :param out_dois: Name of the DOIs output file
    :param manual_biblio: Name of the BibTex file containing additional bibliography
    """
    # Find the publication on Google Scholar
    pub = ScholarPublication(pub_title)
    # Define the name of the produced csv file
    csvfile = out + '.csv'
    # Convert the list of the citing papers to csv
    bibstring_to_csv(pub.citations_bibstring, out=csvfile)
    # Add the DOIs to the csv file
    doi_list = add_dois(csvfile)
    # Write list of found DOIs in a file
    write_list_in_file(doi_list, out=out_dois)
    # Add the DOIs to the bibtex string
    pub.add_citations_doi_from_csv(csvfile)
    # Merge manually provided bib file
    if manual_biblio is not None:
        pub.merge_bibfile_with_citations(manual_biblio)
    # Write bibtex file of citing papers
    pub.write_citations_bibfile(out + '.bib')


def write_list_in_file(l, out='out_list.txt'):
    """
    Write the element of a list in a file, one element per line.
    :param l: list to write
    :param out: output file name
    """
    with open(out, 'w') as f:
        for elem in l:
            f.write(elem + '\n')


if __name__ == '__main__':
    main()
