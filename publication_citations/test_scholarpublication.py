import pytest
import os
from unittest import mock

from scholarly.data_types import PublicationSource

from scholarpublication import ScholarPublication


@pytest.mark.scholarfetch
def test_citationslist():
    """
    Tests if the list of citations is created
    """
    title = "Left atrial hypertrophy increases P-wave terminal force through amplitude but not duration"
    publication = ScholarPublication(title)
    assert isinstance(publication.citations, list)

# Example of citations' list
citations_example = [
    {'container_type': 'Publication', 'source': PublicationSource.PUBLICATION_SEARCH_SNIPPET, 'bib':
        {'title': 'Influence of left atrial size on P-wave morphology: differential effects of dilation and hypertrophy', 'author': ['R Andlauer', 'G Seemann', 'L Baron', 'O Dössel'], 'pub_year': '2018', 'venue': 'EP …', 'abstract': 'Aims Chronic left atrial enlargement (LAE) increases the risk of atrial fibrillation. Electrocardiogram (ECG) criteria might provide a means to diagnose LAE and identify patients at risk; however, current criteria perform poorly. We seek to characterize the'},
     'filled': False, 'gsrank': 1, 'pub_url': 'https://academic.oup.com/europace/article-abstract/20/suppl_3/iii36/5202170', 'author_id': ['', 'Nb58eqMAAAAJ', '', '0khlmQkAAAAJ'], 'num_citations': 20, 'url_scholarbib': '/scholar?q=info:j3xxNfqp3O4J:scholar.google.com/&output=cite&scirp=0&hl=en', 'url_add_sclib': '/citations?hl=en&xsrf=&continue=/scholar%3Fhl%3Den%26as_sdt%3D5,33%26sciodt%3D0,33%26cites%3D8600847612141600543%26scipsc%3D&citilm=1&json=&update_op=library_add&info=j3xxNfqp3O4J&ei=XknQYJ6GFMvhmQGx5IX4Cw', 'citedby_url': '/scholar?cites=17211818768006741135&as_sdt=5,33&sciodt=0,33&hl=en', 'url_related_articles': '/scholar?q=related:j3xxNfqp3O4J:scholar.google.com/&scioq=&hl=en&as_sdt=5,33&sciodt=0,33', 'eprint_url': 'https://academic.oup.com/europace/article/20/suppl_3/iii36/5202170'},
    {'container_type': 'Publication', 'source': PublicationSource.PUBLICATION_SEARCH_SNIPPET, 'bib':
        {'title': 'Maximization of left atrial information through the optimization of ECG lead systems', 'author': ['A Loewe', 'S Debatin', 'G Lenis'], 'pub_year': '2017', 'venue': '2017 Computing in …', 'abstract': 'Atrial fibrillation and atrial flutter are the most common atrial arrhythmias placing a heavy burden on patients and posing a challenge on healthcare systems. If patients at risk to develop atrial arrhythmias can be identified at an early stage, the arrhythmia incidence can'},
     'filled': False, 'gsrank': 3, 'pub_url': 'https://ieeexplore.ieee.org/abstract/document/8331536/', 'author_id': ['dLThgu0AAAAJ', '', ''], 'num_citations': 0, 'url_scholarbib': '/scholar?q=info:kWkqREH8S54J:scholar.google.com/&output=cite&scirp=2&hl=en', 'url_add_sclib': '/citations?hl=en&xsrf=&continue=/scholar%3Fhl%3Den%26as_sdt%3D5,33%26sciodt%3D0,33%26cites%3D8600847612141600543%26scipsc%3D&citilm=1&json=&update_op=library_add&info=kWkqREH8S54J&ei=XknQYJ6GFMvhmQGx5IX4Cw', 'url_related_articles': '/scholar?q=related:kWkqREH8S54J:scholar.google.com/&scioq=&hl=en&as_sdt=5,33&sciodt=0,33', 'eprint_url': 'https://www.researchgate.net/profile/Axel_Loewe/publication/324049187_Maximization_of_Left_Atrial_Information_through_the_Optimization_of_ECG_Lead_Systems/links/5abcd43daca27222c75403c8/Maximization-of-Left-Atrial-Information-through-the-Optimization-of-ECG-Lead-Systems.pdf'},
    {'container_type': 'Publication', 'source': PublicationSource.PUBLICATION_SEARCH_SNIPPET, 'bib':
        {'title': 'Relationship of P Terminal Force V1 on Electrocardiogram with Left Atrial Function in Chronic Kidney Failure Patients on Hemodialysis', 'author': ['K Apshanti', 'PPR Gharini', 'H Mumpuni'], 'pub_year': 'NA', 'venue': 'ACI (Acta Cardiologia …', 'abstract': 'Background: Chronic kidney failure is a worldwide public health problem. Cardiovascular disease is a common complication and the main cause of mortality in this population. Impaired left atrial function is an early marker of cardiovascular involvement and a'},
     'filled': False, 'gsrank': 4, 'pub_url': 'https://journal.ugm.ac.id/jaci/article/view/50216', 'author_id': ['', 'GwML71cAAAAJ', 'fkij9vkAAAAJ'], 'num_citations': 0, 'url_scholarbib': '/scholar?q=info:0Yf4hmVq-y8J:scholar.google.com/&output=cite&scirp=3&hl=en', 'url_add_sclib': '/citations?hl=en&xsrf=&continue=/scholar%3Fhl%3Den%26as_sdt%3D5,33%26sciodt%3D0,33%26cites%3D8600847612141600543%26scipsc%3D&citilm=1&json=&update_op=library_add&info=0Yf4hmVq-y8J&ei=XknQYJ6GFMvhmQGx5IX4Cw', 'url_related_articles': '/scholar?q=related:0Yf4hmVq-y8J:scholar.google.com/&scioq=&hl=en&as_sdt=5,33&sciodt=0,33', 'eprint_url': 'https://journal.ugm.ac.id/jaci/article/download/50216/25708'}
]


# Mock __init__ method for ShcolarPublication class for test purposes
def new_init(self, publication_title):
        self.title = publication_title
        self.scholarPublication = None
        self._citations = citations_example.copy()
        self._citations_bibstring = None
        self.blacklist_dois = ['10.21203/rs.3.rs-254560/v1']
        self.blacklist_titles = ["Maximization of left atrial information through the optimization of ECG lead systems"]


# Using mock __init__ method to avoid to fetch from Google Scholar
with mock.patch.object(ScholarPublication, '__init__', new_init):
    publication = ScholarPublication('mock title')

    # citations_bibstring of the test ScholarPublication object
    res_bibstring = """@article{andlauer2018influence,
abstract = {Aims Chronic left atrial enlargement (LAE) increases the risk of atrial fibrillation. Electrocardiogram (ECG) criteria might provide a means to diagnose LAE and identify patients at risk; however, current criteria perform poorly. We seek to characterize the },
author = {Andlauer, Robin and Seemann, Gunnar and Baron, Lukas and D{\\"o}ssel, Olaf and Kohl, Peter and Platonov, Pyotr and Loewe, Axel},
journal = {EP Europace},
number = {suppl\_3},
pages = {iii36--iii44},
pub_year = {2018},
publisher = {Oxford University Press},
title = {Influence of left atrial size on P-wave morphology: differential effects of dilation and hypertrophy},
venue = {EP …},
volume = {20}
}


@inproceedings{loewe2017maximization,
abstract = {Atrial fibrillation and atrial flutter are the most common atrial arrhythmias placing a heavy burden on patients and posing a challenge on healthcare systems. If patients at risk to develop atrial arrhythmias can be identified at an early stage, the arrhythmia incidence can},
author = {Loewe, Axel and Debatin, Sebastian and Lenis, Gustavo and D{\\"o}ssel, Olaf},
booktitle = {2017 Computing in Cardiology (CinC)},
organization = {IEEE},
pages = {1--4},
pub_year = {2017},
title = {Maximization of left atrial information through the optimization of ECG lead systems},
venue = {2017 Computing in …}
}


@article{apshanti5relationship,
abstract = {Background: Chronic kidney failure is a worldwide public health problem. Cardiovascular disease is a common complication and the main cause of mortality in this population. Impaired left atrial function is an early marker of cardiovascular involvement and a},
author = {Apshanti, Kartika and Gharini, Putrika Prastuti Ratna and Mumpuni, Hasanah},
journal = {ACI (Acta Cardiologia Indonesiana)},
number = {2},
pages = {90--100},
pub_year = {NA},
title = {Relationship of P Terminal Force V1 on Electrocardiogram with Left Atrial Function in Chronic Kidney Failure Patients on Hemodialysis},
venue = {ACI (Acta Cardiologia …},
volume = {5}
}


"""

    bibstring_with_dois = """@article{andlauer2018influence,
    author = {Andlauer, Robin and Seemann, Gunnar and Baron, Lukas and D{\\"o}ssel, Olaf and Kohl, Peter and Platonov, Pyotr and Loewe, Axel},
    abstract = "Aims Chronic left atrial enlargement (LAE) increases the risk of atrial fibrillation. Electrocardiogram (ECG) criteria might provide a means to diagnose LAE and identify patients at risk; however, current criteria perform poorly. We seek to characterize the",
    journal = "EP Europace",
    number = "suppl\\\_3",
    pages = "iii36--iii44",
    pub_year = "2018",
    publisher = "Oxford University Press",
    title = "Influence of left atrial size on P-wave morphology: differential effects of dilation and hypertrophy",
    venue = "EP …",
    volume = "20",
    doi = "10.1093/europace/euy231"
}

@inproceedings{loewe2017maximization,
    author = {Loewe, Axel and Debatin, Sebastian and Lenis, Gustavo and D{\\"o}ssel, Olaf},
    abstract = "Atrial fibrillation and atrial flutter are the most common atrial arrhythmias placing a heavy burden on patients and posing a challenge on healthcare systems. If patients at risk to develop atrial arrhythmias can be identified at an early stage, the arrhythmia incidence can",
    booktitle = "2017 Computing in Cardiology (CinC)",
    organization = "IEEE",
    pages = "1--4",
    pub_year = "2017",
    title = "Maximization of left atrial information through the optimization of ECG lead systems",
    venue = "2017 Computing in …"
}

@article{apshanti5relationship,
    author = "Apshanti, Kartika and Gharini, Putrika Prastuti Ratna and Mumpuni, Hasanah",
    abstract = "Background: Chronic kidney failure is a worldwide public health problem. Cardiovascular disease is a common complication and the main cause of mortality in this population. Impaired left atrial function is an early marker of cardiovascular involvement and a",
    journal = "ACI (Acta Cardiologia Indonesiana)",
    number = "2",
    pages = "90--100",
    pub_year = "NA",
    title = "Relationship of P Terminal Force V1 on Electrocardiogram with Left Atrial Function in Chronic Kidney Failure Patients on Hemodialysis",
    venue = "ACI (Acta Cardiologia …",
    volume = "5"
}
"""

    mock_csv_dois = """title,doi
"Influence of left atrial size on P-wave morphology: differential effects of dilation and hypertrophy","10.1093/europace/euy231"
"Relationship of P Terminal Force V1 on Electrocardiogram with Left Atrial Function in Chronic Kidney Failure Patients on Hemodialysis",
"""

    def test_dois_from_csv():
        """
        Tests the add_citations_doi_from_csv method
        """
        publication._citations_bibstring = res_bibstring
        mock_csv = mock_csv_dois
        expected_bibstring = bibstring_with_dois
        mock_open = mock.mock_open(read_data=mock_csv)
        with mock.patch('builtins.open', mock_open):
            publication.add_citations_doi_from_csv('mock_csv')
            assert publication.citations_bibstring == expected_bibstring


    def test_blacklisted_dois():
        """
        Tests the removing of blacklisted dois
        """
        publication._citations_bibstring = res_bibstring
        mock_csv = mock_csv_dois
        publication.blacklist_dois = ['10.1093/europace/euy231']

        expected_bibstring = """@inproceedings{loewe2017maximization,
    author = {Loewe, Axel and Debatin, Sebastian and Lenis, Gustavo and D{\\"o}ssel, Olaf},
    abstract = "Atrial fibrillation and atrial flutter are the most common atrial arrhythmias placing a heavy burden on patients and posing a challenge on healthcare systems. If patients at risk to develop atrial arrhythmias can be identified at an early stage, the arrhythmia incidence can",
    booktitle = "2017 Computing in Cardiology (CinC)",
    organization = "IEEE",
    pages = "1--4",
    pub_year = "2017",
    title = "Maximization of left atrial information through the optimization of ECG lead systems",
    venue = "2017 Computing in …"
}

@article{apshanti5relationship,
    author = "Apshanti, Kartika and Gharini, Putrika Prastuti Ratna and Mumpuni, Hasanah",
    abstract = "Background: Chronic kidney failure is a worldwide public health problem. Cardiovascular disease is a common complication and the main cause of mortality in this population. Impaired left atrial function is an early marker of cardiovascular involvement and a",
    journal = "ACI (Acta Cardiologia Indonesiana)",
    number = "2",
    pages = "90--100",
    pub_year = "NA",
    title = "Relationship of P Terminal Force V1 on Electrocardiogram with Left Atrial Function in Chronic Kidney Failure Patients on Hemodialysis",
    venue = "ACI (Acta Cardiologia …",
    volume = "5"
}
"""
        mock_open = mock.mock_open(read_data=mock_csv)
        with mock.patch('builtins.open', mock_open):
            publication.add_citations_doi_from_csv('mock_csv')
            assert publication.citations_bibstring == expected_bibstring


    def test_merge_bibfile_with_citations():
        """
        Tests the merge_bibfile_with_citations method
        """
        publication._citations_bibstring = res_bibstring
        mock_bibfile = """@inproceedings{loewe2017maximization,
author = {Loewe, Axel and Debatin, Sebastian and Lenis, Gustavo and D{\\"o}ssel, Olaf},
abstract = "Atrial fibrillation and atrial flutter are the most common atrial arrhythmias placing a heavy burden on patients and posing a challenge on healthcare systems. If patients at risk to develop atrial arrhythmias can be identified at an early stage, the arrhythmia incidence can",
booktitle = "2017 Computing in Cardiology (CinC)",
organization = "IEEE",
pages = "1--4",
pub_year = "2017",
title = "Maximization of left atrial information through the optimization of ECG lead systems",
venue = "2017 Computing in …"
}

@book{2021book,
author = "An author",
title = "A title"
}
"""

        expected_bibstring = """@inproceedings{loewe2017maximization,
    author = {Loewe, Axel and Debatin, Sebastian and Lenis, Gustavo and D{\\"o}ssel, Olaf},
    abstract = "Atrial fibrillation and atrial flutter are the most common atrial arrhythmias placing a heavy burden on patients and posing a challenge on healthcare systems. If patients at risk to develop atrial arrhythmias can be identified at an early stage, the arrhythmia incidence can",
    booktitle = "2017 Computing in Cardiology (CinC)",
    organization = "IEEE",
    pages = "1--4",
    pub_year = "2017",
    title = "Maximization of left atrial information through the optimization of ECG lead systems",
    venue = "2017 Computing in …"
}

@book{2021book,
    author = "author, An",
    title = "A title"
}

@article{andlauer2018influence,
    author = {Andlauer, Robin and Seemann, Gunnar and Baron, Lukas and D{\\"o}ssel, Olaf and Kohl, Peter and Platonov, Pyotr and Loewe, Axel},
    abstract = "Aims Chronic left atrial enlargement (LAE) increases the risk of atrial fibrillation. Electrocardiogram (ECG) criteria might provide a means to diagnose LAE and identify patients at risk; however, current criteria perform poorly. We seek to characterize the",
    journal = "EP Europace",
    number = "suppl\\\_3",
    pages = "iii36--iii44",
    pub_year = "2018",
    publisher = "Oxford University Press",
    title = "Influence of left atrial size on P-wave morphology: differential effects of dilation and hypertrophy",
    venue = "EP …",
    volume = "20"
}

@article{apshanti5relationship,
    author = "Apshanti, Kartika and Gharini, Putrika Prastuti Ratna and Mumpuni, Hasanah",
    abstract = "Background: Chronic kidney failure is a worldwide public health problem. Cardiovascular disease is a common complication and the main cause of mortality in this population. Impaired left atrial function is an early marker of cardiovascular involvement and a",
    journal = "ACI (Acta Cardiologia Indonesiana)",
    number = "2",
    pages = "90--100",
    pub_year = "NA",
    title = "Relationship of P Terminal Force V1 on Electrocardiogram with Left Atrial Function in Chronic Kidney Failure Patients on Hemodialysis",
    venue = "ACI (Acta Cardiologia …",
    volume = "5"
}
"""
        mock_open = mock.mock_open(read_data=mock_bibfile)
        with mock.patch('builtins.open', mock_open):
            publication.merge_bibfile_with_citations('mock_bibfile')
        assert publication.citations_bibstring == expected_bibstring


    def test_remove_blacklisted_titles():
        """
        Tests the remove_blacklisted_citations method for titles
        """
        publication.remove_blacklisted_citations(field='title', values=publication.blacklist_titles)
        assert len(publication.citations) == 2
        assert publication.citations[0] == citations_example[0]
        assert publication.citations[1] == citations_example[2]






