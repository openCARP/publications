import csv
import io
import random
import string

from scholarly import scholarly, ProxyGenerator
from fp.fp import FreeProxy
from pybtex.database.input.bibtex import Parser
from pybtex.database.output.bibtex import Writer


class ScholarPublication:

    """
    This class describes a publication retrieved on Google Scholar.
    It should be instantiated by providing the title of the publication.
    The publication taken in account will be the first result returned by
    Google Scholar by searching this title.
    """

    def __init__(self, publication_title, blacklist_dois=[], blacklist_titles=[]):
        # Instantiation of the proxy
        #pg = ProxyGenerator()

        # Using fp.FreeProxy
        #pg.FreeProxies()

        # Using custom fp.FreeProxy
        #country_ids = ['JP']
        #proxy = FreeProxy(rand=True, timeout=1, country_id=country_ids).get()
        #pg.SingleProxy(http=proxy, https=proxy)

        # Using KIT proxy
        #proxy = "http://proxy.kit.edu:3128/"
        #pg.SingleProxy(http=proxy, https=proxy)

        #scholarly.use_proxy(pg)
        self.title = publication_title
        # The actual publication is the first result returned by the Scholar search
        self.scholarPublication = next(scholarly.search_pubs(self.title))
        self._citations = None
        self._citations_bibstring = None
        self.blacklist_dois = blacklist_dois
        self.blacklist_titles = blacklist_titles

    @property
    def citations(self):
        """
        List of publications which cite the publication.
        """
        if self._citations is None:
            try:
                self._citations = list(scholarly.citedby(self.scholarPublication))
                # Remove blacklisted publications
                self.remove_blacklisted_citations(field='title', values=self.blacklist_titles)
            # scholarly raises a KeyError if the publication is not cited by anyone
            except KeyError:
                print("No citations found.")
                self._citations = []
        return self._citations

    @property
    def citations_bibstring(self):
        """
        Returns the bibtex string describing the citing papers.
        """
        if self._citations_bibstring is None:
            # Bibtex string containing the citations
            s = ''
            # List of titles (used to check and ignore duplicates)
            titles = []
            # List of Bibtex identifiers (identifiers are modified in case of duplicates)
            bibtex_ids = []
            for citation in self.citations:
                # If several publication have the same title, duplicates are ignored
                if citation['bib']['title'] not in titles:
                    bib_citation = scholarly.bibtex(citation)
                    # Extract Bibtex identifier
                    bibtex_id = bib_citation.split('{')[1].split(',')[0]
                    # If the identifier already exists, a random letter is added at the end.
                    if bibtex_id in bibtex_ids:
                        bibtex_id += random.choice(string.ascii_lowercase)
                        bib_citation = bib_citation.replace(bibtex_id[:-1], bibtex_id)
                    # Paper is added to the global Bibtex string
                    s += bib_citation + '\n'
                    # Register the id of the Bibtex entry in the list of Bibtex identifiers
                    bibtex_ids.append(bibtex_id)
                    titles.append(citation['bib']['title'])
            self._citations_bibstring = s
        return self._citations_bibstring

    def remove_blacklisted_citations(self, field='title', values=[]):
        del_ind = []
        for ind, cit in enumerate(self._citations):
           if cit['bib'][field] in values:
                del_ind.append(ind)
        del_ind.sort(reverse=True)
        for ind in del_ind:
           del self._citations[ind]

    def add_citations_doi_from_csv(self, csvfile):
        """
        The dois of the citing papers are added to their bibtex string from
        a csv file.
        The name of the citing papers have to be given in a field called 'title'
        and the DOIs in a field called 'doi'.
        In this function, the papers whose dois are blacklisted are removed from the BibTex string.

        :param csvfile: The CSV file containing the DOIs
        """
        parser = Parser()
        bib_data = parser.parse_string(self.citations_bibstring)
        with open(csvfile, "r") as f:
            del_keys = []
            for key, value in bib_data.entries.items():
                f.seek(0)
                reader = csv.DictReader(f)
                for line in reader:
                    if 'doi' not in value.fields and line['doi'] != '' and line['title'] == value.fields['title']:
                        value.fields['doi'] = line['doi']
                        if line['doi'] in self.blacklist_dois:
                            del_keys.append(key)
                        break
        for key in del_keys:
            del bib_data.entries[key]
        writer = Writer()
        with io.StringIO() as f:
            writer.write_stream(bib_data, f)
            f.seek(0)
            self._citations_bibstring = f.read()

    def merge_bibstring_with_citations(self, bibstring):
        """
        Merges the BibTex string of the publication's citations with the content of the BibTex string given as parameter.
        Checking for duplicates is made on publications' titles. In case of duplicate, the input BibTex string content is
        kept.
        :param bibstring: BibTex string to be merged with self.citations_publications
        """
        parser = Parser()
        input_data = parser.parse_string(bibstring)
        parser_s = Parser()
        citations_data = parser_s.parse_string(self.citations_bibstring)
        # BibTex entries from the citations list are merged with the ones from the BibTex file only if
        # they are not duplicates.
        for key_c, value_c in citations_data.entries.items():
            is_duplicate = False
            for key_f, value_f in input_data.entries.items():
                if value_f.fields['title'] == value_c.fields['title']:
                    is_duplicate = True
                    break
            if not is_duplicate:
                input_data.entries[key_c] = value_c
        # The citations BibTex string is then updated
        writer = Writer()
        with io.StringIO() as f:
            writer.write_stream(input_data, f)
            f.seek(0)
            self._citations_bibstring = f.read()

    def merge_bibfile_with_citations(self, bibfile):
        """
        Merges the BibTex string of the publication's citations with the content of the BibTex file given as parameter.
        Checking for duplicates is made on publications' titles. In case of duplicate, the entry in the BibTex file is
        kept.
        :param bibfile: Path to the BibTex file to be merged
        """
        with open(bibfile, 'r') as f:
            s = f.read()
            self.merge_bibstring_with_citations(s)

    def write_citations_bibfile(self, filename=None):
        """
        Writes the bibtex file containing the information about the citing papers.

        :param filename: name of the output file
        """
        if filename is None:
            filename = 'biblio.bib'
        with open(filename, 'w') as f:
            f.write(self._citations_bibstring)



