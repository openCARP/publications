fp>=0.2
scholarly>=1.2.0
pytest>=6.2.4
pybtex>=0.24.0
levenshtein>=0.12.0
