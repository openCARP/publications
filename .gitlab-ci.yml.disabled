variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PIPELINE: publications
  PIPELINE_SOURCE: .
  PIPELINE_CSL: frontiers.csl
  GRAV_PATH: opencarp.org

cache:
  paths:
  - .cache/pip

publish_to_web:
  image: python:3.7
  before_script:
  - apt-get update
  - apt-get -y install pandoc pandoc-citeproc
  - git config --global user.name "openCARP pipeline"
  - git config --global user.email opencarp-admins@lists.kit.edu
  - git clone https://opencarp-admin:${PRIVATE_TOKEN}@git.opencarp.org/infrastructure/opencarp.org.git
  - pip install git+https://git.opencarp.org/openCARP/openCARP-CI.git
  script:
    - run_bibtex_pipeline
  only:
    variables:
      - $CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "schedule"    
  after_script:
  - cd opencarp.org && git add . && git commit -m '(opencarp-pipeline) Automatic Commit' && git push

update_publications:
  image: python:3.7
  rules:
  - if: $CI_PIPELINE_SOURCE == "schedule"
  before_script:
  - git clone https://opencarp-admin:${PRIVATE_TOKEN}@git.opencarp.org/openCARP/publications.git && cd publications
  - git config --global user.name "openCARP pipeline"
  - git config --global user.email opencarp-admins@lists.kit.edu
  - pip install -r requirements.txt
  - patch /usr/local/lib/python3.7/site-packages/fake_useragent/utils.py publication_citations/utils.patch
  script:
  - python find_opencarp_citations.py --push --skip-carpentry

test:
  image: python:3.7
  before_script:
  - pip install -r publication_citations/requirements.txt
  script:
  - python -m pytest -m 'not scholarfetch'
  rules:
  - if: $CI_PIPELINE_SOURCE != "schedule"
